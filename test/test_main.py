import unittest
from test.main import main
from random import randrange

class TestMain(unittest.TestCase):

    def test_positive_int(self):
        num = randrange(0,1000)
        self.assertEqual(main(num), 1)

    def test_negative_int(self):
        num = randrange(-1000,-1)
        self.assertEqual(main(num), -1)

    def test_str(self):
        s = "string"
        self.assertEqual(main(s), 2)

    def test_bool(self):
        b = True
        self.assertEqual(main(b), 0)
