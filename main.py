# we want our hello world file to return an exit code based on the first arg
# int >= 0: 1
# int <  0: -1
# str : 2
# other : 0

def main(arg):
    if isinstance(arg, bool):
        return 0

    if isinstance(arg, int):
        if arg <= 0:
            return -1
        if arg > 0:
            return 1

    if isinstance(arg, str):
        return 2

    return 0
